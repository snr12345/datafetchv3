
# coding: utf-8

# In[14]:


# dont overwrite if file exists - check this - DONE
# should download for multipe stocks - DONE
# hold last 30 mins or last oneday intraday data in an array
# While putting data into file, add analysis function as well
# implement simple stragey - high breakout
# calculate average of tempdata for 1 min
# download old intraday data and save in separate file


# In[15]:


from IPython.core.display import display, HTML
import requests
import os
from bs4 import BeautifulSoup
import numpy as np
import unidecode as uc
import re
import threading
from datetime import datetime
import xlrd
import xlwt
from xlutils.copy import copy
from pytz import timezone
from tzlocal import get_localzone
import os.path
from os import path
from os import listdir
from os.path import isfile, join

#os.environ["HTTPS_PROXY"] = "http://username:pass@192.168.1.107:3128"


# headers = {'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36'}
headers = {'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Safari/605.1.15'}


    
def getBasicQuotes(Stock,timeused):
    source_code = requests.get(Stock["URL"], headers=headers, timeout=15)
    plain_text = source_code.text
    soup = BeautifulSoup(plain_text,"html.parser")
    TimeVal = getTimeFormatted(timeused)
    
    PriceVal = getPrice(soup)
    OpenVal = getOpen(soup)
    HighVal = getHigh(soup)
    LowVal = getLow(soup)
    VolumeVal = getVolume(soup)
    
    
    data ={
    "Date": TimeVal,
    "Price": PriceVal,
    "Open": OpenVal,
    "High": HighVal,
    "Low": LowVal,
    "Volume": VolumeVal,
    "timeused":timeused
    }
    
    return data

# headers = {'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36'}
headers = {'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Safari/605.1.15'}
    
def getPrice(soupObj):
    htmlthere = soupObj.select("#quotes_summary_current_data > div.left > div.inlineblock > div.top.bold.inlineblock >span")[0].contents[0]
    htmlthere = htmlthere.replace(",","")
    htmlthere = float(htmlthere)
    return htmlthere


def getOpen(soupObj):
    htmlthere = soupObj.select("div.overviewDataTable.overviewDataTableWithTooltip >div>span")[7].contents[0]
    htmlthere = htmlthere.replace(",","")
    htmlthere = float(htmlthere)    
    return htmlthere

def getHigh(soupObj):
    htmlthere = soupObj.select("#quotes_summary_secondary_data > div > ul > li:nth-child(3) > span:nth-child(2) >span")[1].contents[0]
    htmlthere = htmlthere.replace(",","")
    htmlthere = float(htmlthere)
    return htmlthere

def getLow(soupObj):
    htmlthere = soupObj.select("#quotes_summary_secondary_data > div > ul > li:nth-child(3) > span:nth-child(2) >span")[0].contents[0]
    htmlthere = htmlthere.replace(",","")
    htmlthere = float(htmlthere)
    return htmlthere

def getVolume(soupObj):
    htmlthere = soupObj.select("div.overviewDataTable.overviewDataTableWithTooltip >div>span")[13].contents[0]
    htmlthere = htmlthere.replace(",","")
    htmlthere = float(htmlthere)    
    return htmlthere

def getTimeFormatted(ist_now):
    mdata = ist_now.strftime("%d/%m/%Y %H:%M:%S")
    return str(mdata)

def getTime():
#     nowtime = datetime.now()
#     return nowtime

    tz = timezone('Asia/Calcutta')
    ist_now = datetime.now(tz)
    return ist_now


def getTodate():
    mt = getTime()
    dt = mt.strftime("%d.%m.%Y")
    return dt


# In[16]:


def getStocksInput():
    stockslist=[]
    loc = ("stocksinput.csv") 

    wb = xlrd.open_workbook(loc) 
    sheet = wb.sheet_by_index(0)

    for i in range(sheet.nrows): 
    # Extracting number of columns 
        if i==0:
            continue
        print(sheet.cell_value(i, 0))
        datainput = {
        "Symbol": sheet.cell_value(i, 0),
        "URL": sheet.cell_value(i, 1),
        "num":0,
        "stoploop":0,
        "logfile":"temp.xls",
        "livedatafile":"Demo_livedata.xls",
        "tempdata":[]
        }
        
        stockslist.append(datainput)
        
    return stockslist


# In[17]:


# ist_now.second


# In[18]:


def CreateIfNotExists(filename):
    if path.exists(filename):
        return 0
    wbx = xlwt.Workbook()
    sheet1 = wbx.add_sheet('Sheet 1')
    wbx.save(filename)
    
def CheckAndCreateFolder(folderPath):
    if not os.path.exists(folderPath):
        os.makedirs(folderPath)

def getFileList(foldersPath):    
    onlyfiles = [f for f in listdir(foldersPath) if isfile(join(foldersPath, f)) and f.find("livedata")==0]
    onlyfiles.sort()
    return onlyfiles

def getLastFileName(Stock):
    names=[]
    for file in getFileList(Stock["folderPath"]):
        fn = file.replace(".xls","")
        pref = "livedata_"+Stock["Symbol"]+"_"
        print('pref')
        print(pref)
        print('fn')
        print(fn)
        fn = fn.replace("livedata_"+Stock["Symbol"]+"_","")
        print('replaced is')
        print(fn)
        names.append(int(fn))
    if len(names)==0:
        return ""
    names.sort()
    return names[len(names)-1]


# In[19]:


def initialize(Stock):
    dt = getTodate()
    Stock["folderPath"] = "Data/"+Stock["Symbol"]+"/"+dt
    
    CheckAndCreateFolder(Stock["folderPath"])
    
    lastName = getLastFileName(Stock)
    currentFileName=1
    if lastName!="":
        currentFileName=int(lastName)+1
    
    
    livedatafile ="Data/"+Stock["Symbol"]+"/"+dt+"/livedata_"+Stock["Symbol"]+"_"+str(currentFileName)+".xls" 
    logfile ="Data/"+Stock["Symbol"]+"/"+dt+"/log_"+Stock["Symbol"]+"_"+str(currentFileName)+".xls" 

    
    Stock["logfile"]=logfile
    Stock["livedatafile"]=livedatafile
    

    CreateIfNotExists(Stock["logfile"])
    CreateIfNotExists(Stock["livedatafile"])


# In[20]:


def appendLogSpace(workbookpath,data):
    rb = xlrd.open_workbook(workbookpath)
    r_sheet = rb.sheet_by_index(0) 
    r = r_sheet.nrows
    wb = copy(rb) 
    sheet = wb.get_sheet(0) 
    sheet.write(r,0,str(data["Date"]))
    sheet.write(r,1,data["Price"])
    sheet.write(r,2,data["Open"])
    sheet.write(r,3,data["High"])
    sheet.write(r,4,data["Low"])
    sheet.write(r,5,data["Volume"])
    wb.save(workbookpath)
    print('logged to :'+workbookpath)


def appendWorkSpace(workbookpath,data):
    rb = xlrd.open_workbook(workbookpath)
    r_sheet = rb.sheet_by_index(0) 
    r = r_sheet.nrows
    wb = copy(rb) 
    sheet = wb.get_sheet(0) 
    sheet.write(r,0,str(data["Date"]))
    sheet.write(r,1,data["Price"])
    sheet.write(r,2,data["Open"])
    sheet.write(r,3,data["High"])
    sheet.write(r,4,data["Low"])
    sheet.write(r,5,data["Volume"])
    wb.save(workbookpath)
    print('Wrote to :'+workbookpath)

    


# In[21]:


def calculateAvgMetrics(alldata):
    highval=0
    openval=0
    closeval=0
    lowval=0
    avgdata=alldata[0]
    return avgdata


# In[22]:


def AnalyzeOrSave(Stock,data):
    
#     save the data for loggin
    appendLogSpace(Stock["logfile"],data)
    
    
    if len(Stock["tempdata"])!=0:
        lastdatasaved = Stock["tempdata"][len(Stock["tempdata"])-1]
    else:
        lastdatasaved=0
    
    if lastdatasaved!=0 and lastdatasaved["timeused"].minute != data["timeused"].minute:
        avgdata = calculateAvgMetrics(Stock["tempdata"])
        appendWorkSpace(Stock["livedatafile"],avgdata)
        Stock["tempdata"]=[]
        print(" === saved to excel file"+str(data["timeused"]))
    
    Stock["tempdata"].append(data) 
    print("price added to temp data"+str(data["timeused"]))


# In[23]:




def startFetching(Stock):
    timenow = getTime()
#     print('before validation, seconds are:'+str(timenow.second))
#     if timenow.second%2==0:
#     print('after validation, seconds are:'+str(timenow.second))
    data = getBasicQuotes(Stock,timenow)
    AnalyzeOrSave(Stock,data)
    Stock["num"]=Stock["num"]+1
    print('fetched '+str(Stock["num"])+'  times')
    if Stock["stoploop"]==0:
        threading.Timer(0.5, startFetching,[Stock]).start()


# In[24]:


def RunAStock(Stock):
    initialize(Stock)
    startFetching(Stock)


# In[25]:


for Stock in getStocksInput():
    t1 = threading.Thread(target=RunAStock, args=(Stock,)) 
    t1.start() 
#     RunAStock(Stock)


# In[ ]:


# stockslist[0]["stoploop"]=1
# stockslist[1]["stoploop"]=1

