#!/usr/bin/env python
# coding: utf-8

# In[1]:





# In[14]:


# dont overwrite if file exists - check this - DONE
# should download for multipe stocks - DONE
# hold last 30 mins or last oneday intraday data in an array
# While putting data into file, add analysis function as well
# implement simple stragey - high breakout
# calculate average of tempdata for 1 min
# download old intraday data and save in separate file


# In[15]:
print('starting..')

from IPython.core.display import display, HTML
import requests
import os
from bs4 import BeautifulSoup
import numpy as np
import unidecode as uc
import re
import threading
from datetime import datetime
import xlrd
import xlwt
from xlutils.copy import copy
from pytz import timezone
from tzlocal import get_localzone
import os.path
from os import path
from os import listdir
from os.path import isfile, join
import pandas as pd
from datetime import datetime
import time
from multiprocessing import Process, Pool
import asyncio


#os.environ["HTTPS_PROXY"] = "http://username:pass@192.168.1.107:3128"


print('imported libs..')

# In[2]:


# headers = {'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36'}
# headers = {'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Safari/605.1.15'}
headers = {'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Safari/605.1.15'}


print('declaring..')   
def getBasicQuotes(Stock,timeused):
    
    source_code = requests.get("https://www.nseindia.com/live_market/dynaContent/live_watch/get_quote/GetQuote.jsp?symbol=TCS&illiquid=0&smeFlag=0&itpFlag=0", headers=headers, timeout=60)
    plain_text = source_code.text
    # plain_text = '<html>\r\n<head><title>406 Not Acceptable</title></head>\r\n<body bgcolor="white">\r\n<center><h1>406 Not Acceptable</h1></center>\r\n<hr><center>nginx/1.14.0</center>\r\n</body>\r\n</html>\r\n'
    soup = BeautifulSoup(plain_text,"html.parser")

    TimeVal = getTimeFormatted(timeused)

    # PriceVal = getPrice(soup)
    # OpenVal = getOpen(soup)
    # HighVal = getHigh(soup)
    # LowVal = getLow(soup)
    # VolumeVal = getVolume(soup)
    
    data ={
    "Date": TimeVal,
    "Price": "100",
    "Open": "101",
    "High": "102",
    "Low": "103",
    "Volume": "2000",
    "timeused":timeused
    }
    
    return data


    
def getPrice(soupObj):
    htmlthere = soupObj.select("span#lastPrice")
    
    return htmlthere


def getOpen(soupObj):
    htmlthere = soupObj.select("span#lastPrice")
    
    return htmlthere

def getHigh(soupObj):
    htmlthere = soupObj.select("span#lastPrice")
    
    return htmlthere

def getLow(soupObj):
    htmlthere = soupObj.select("span#lastPrice")
    
    return htmlthere

def getVolume(soupObj):
    htmlthere = soupObj.select("span#lastPrice")
    
    return htmlthere

def getTimeFormatted(ist_now):
    mdata = ist_now.strftime("%d/%m/%Y %H:%M:%S")
    return str(mdata)

def getTime():
#     nowtime = datetime.now()
#     return nowtime

    tz = timezone('Asia/Calcutta')
    ist_now = datetime.now(tz)
    return ist_now


def getTodate():
    mt = getTime()
    dt = mt.strftime("%d.%m.%Y")
    return dt

def getTimeStamp():
    dt_obj = time.time()
    millisec = float(dt_obj)*1000
    ms = (int(np.round(millisec)))
    return ms
# In[16]:

print('declaring csv..')   
# In[3]:


def updateToCsv(filename,df):
        df.to_csv(filename, mode='a', header=False,index=False)

def writeToCsvAsync(filename,df):
    if not os.path.isfile(filename):
        df.to_csv(filename,header=True,index=False)
    else: # else it exists so append without writing the header
        df.to_csv(filename, mode='a', header=False,index=False)

def writeToCsvCall(filename,df):
    writeToCsvAsync(filename,df)

def writeToCsv(filename,df):
    writeToCsvAsync(filename,df)
        
def getStocksInput():
    stockslist=[]
    sdata = pd.read_csv("stocksinput.csv",index_col="Symbol")

    for index in sdata.index:
    # Extracting number of columns 
        print(index)
        datainput = {
        "Symbol": index,
        "URL": sdata.loc[index]["URL"],
        "num":0,
        "stoploop":0,
        "logfile":"temp.csv",
        "livedatafile":"Demo_livedata.csv",
        "tempdata":[]
        }
        
        stockslist.append(datainput)
        
    return stockslist


# In[17]:


# ist_now.second


# In[18]:


print('declaring folders')   
def CheckAndCreateFolder(folderPath):
    if not os.path.exists(folderPath):
        os.makedirs(folderPath)

def getFileList(foldersPath):    
    onlyfiles = [f for f in listdir(foldersPath) if isfile(join(foldersPath, f)) and f.find("livedata")==0]
    onlyfiles.sort()
    return onlyfiles



# In[19]:

print('declaring..')   

# In[4]:


def PrepareAFreshTrackerFile(Stock):
    InsertTracker(Stock)
    return Stock
    

#     we dont need to validate and choose, we should use the new files to know that what periods of timelines
# we have data for. So, dont reuse the file if a fresh run executed, instead use a new file, that will clear
# the question on howmuch data we have, so easy to calculate
def ValidFileNameFromTracker(Stock):
    if os.path.isfile(Stock["filetracker"]):
        trackerdata = pd.read_csv(Stock["filetracker"])
        lastrecord = trackerdata.iloc[len(trackerdata.index)-1]
        if int(lastrecord["TotalCount"])<200:
            Stock["livedatafile"]=lastrecord["livedatafile"]
            Stock["StartedAt"]=lastrecord["livedatafile"]
            Stock["TotalCount"]=0
        else:  
            InsertTracker(Stock)
    else:
        InsertTracker(Stock)
    return Stock

def UpdateCount(Stock):
    UpdateCountCall(Stock)

def UpdateCountCall(Stock):
    
    UpdateFile(Stock["filetracker"],"livedatafile",Stock["livedatafile"],"TotalCount",Stock["TotalCount"])
    UpdateFile(Stock["filetracker"],"livedatafile",Stock["livedatafile"],"EndedAt",getTimeFormatted(getTime()))

def UpdateFile(filepath,indexcolumn,indexvalue,updatecolumn,updatevalue):
    filepath = filepath
    filedata = pd.read_csv(filepath)
    
    datafound = filedata[filedata[indexcolumn]==indexvalue]
    
    if len(datafound.index)==0:
        return 0
    
    asindex = datafound.iloc[len(datafound.index)-1]
    df = pd.DataFrame([asindex])
    df[updatecolumn]=updatevalue
    
    filedata.set_index(indexcolumn,inplace=True)
    df.set_index(indexcolumn,inplace=True)
    filedata.update(df)
    filedata.reset_index()
    
    filedata.to_csv(filepath,header=True,index=True)
    
    return 1
print('declaring..')   
# we dont need this function, becuase we need to move to next record, we dont need to worry about ending specific
# record. because every record will have specific count on when it was last updated
def EndTracker(filepath,record):
    filedata = pd.read_csv(filepath)
    afterdrop = filedata.drop(len(filedata.index)-1)
    recorddata = pd.DataFrame([[record["livedatafile"],record["Symbol"],record["StartedAt"],record["EndedAt"],record["TotalCount"]]],columns=filedata.columns.tolist())
    afterdrop=afterdrop.append(recorddata)
    afterdrop.to_csv(filepath,header=True,index=False)

    
def InsertTracker(Stock):
    Stock["livedatafile"]="Data/"+Stock["Symbol"]+"/livedata_"+Stock["Symbol"]+"_"+str(getTimeStamp())+".csv"
    Stock["StartedAt"]=getTimeFormatted(getTime())
    Stock["EndedAt"]=""
    Stock["TotalCount"]=0
    
    datainlist = [Stock["livedatafile"],Stock["Symbol"],Stock["StartedAt"],Stock["EndedAt"],Stock["TotalCount"]]
    columnlist = ["livedatafile","Symbol","StartedAt","EndedAt","TotalCount"];
    
    df = pd.DataFrame([datainlist],columns=columnlist)
    writeToCsv(Stock["filetracker"],df)

print('declaring..')   
def initialize(Stock):
    Stock["folderPath"] = "Data/"+Stock["Symbol"]
    Stock["filetracker"]="Data/"+Stock["Symbol"]+"/filetracker_"+Stock["Symbol"]+".csv" 
    
    CheckAndCreateFolder(Stock["folderPath"])
    
    PrepareAFreshTrackerFile(Stock)
#     ValidFileNameFromTracker(Stock)

    Stock["logfile"] =Stock["livedatafile"].replace("livedata","logdata")


#     CreateIfNotExists(Stock["logfile"])
#     CreateIfNotExists(Stock["livedatafile"])


print('declaring..')   
# In[20]:
def getQuoteLayout():
    objrows = []
    df = pd.DataFrame(objrows,columns=["Date","Price","Open","High","Low","Volume"])
    return df

def appendLogSpace(workbookpath,data):
    datainlist = [str(data["Date"]),data["Price"],data["Open"],data["High"],data["Low"],data["Volume"]]
    columnlist = ["Date","Price","Open","High","Low","Volume"];
    df = pd.DataFrame([datainlist],columns=columnlist)
    writeToCsv(workbookpath,df)
    print('logged to :'+workbookpath)


def appendWorkSpace(workbookpath,data):
    datainlist = [str(data["Date"]),data["Price"],data["Open"],data["High"],data["Low"],data["Volume"]]
    columnlist = ["Date","Price","Open","High","Low","Volume"];
    df = pd.DataFrame([datainlist],columns=columnlist)
    writeToCsv(workbookpath,df)
    print('Live data to :'+workbookpath)

    

print('declaring..')   
# In[5]:



def calculateAvgMetrics(alldata):
    highval=0
    openval=0
    closeval=0
    lowval=0
    avgdata=alldata[0]
    return avgdata


# In[22]:
print('declaring..')   

def AnalyzeOrSave(Stock,data):
    
#     save the data for loggin
    appendLogSpace(Stock["logfile"],data)
    
    
    if len(Stock["tempdata"])!=0:
        lastdatasaved = Stock["tempdata"][len(Stock["tempdata"])-1]
    else:
        lastdatasaved=0
    
    if lastdatasaved!=0 and lastdatasaved["timeused"].minute != data["timeused"].minute:
        avgdata = calculateAvgMetrics(Stock["tempdata"])
        Stock["TotalCount"]=Stock["TotalCount"]+1
        
        if Stock["TotalCount"]>=200:
            Stock=PrepareAFreshTrackerFile(Stock)
        else:
            UpdateCount(Stock)
            
        appendWorkSpace(Stock["livedatafile"],avgdata)
        Stock["tempdata"]=[]
        print(" === saved to excel file"+str(data["timeused"]))
    
    Stock["tempdata"].append(data) 
    print("price added to temp data"+str(data["timeused"]))


# In[23]:

print('declaring..')   


# In[7]:

def AfterRequests(Stock,data):
    AnalyzeOrSave(Stock,data)
    Stock["num"]=Stock["num"]+1
    print('fetched '+str(Stock["num"])+'  times')


def startFetching(Stock):
    timenow = getTime()

#     print('before validation, seconds are:'+str(timenow.second))
#     if timenow.second%2==0:
#     print('after validation, seconds are:'+str(timenow.second))
    data = getBasicQuotes(Stock,timenow)

    AfterRequests(Stock,data)

    # if Stock["stoploop"]==0:
        # because requests are synchronous, this will wait for requests, so no worries
        # this will request again - as soon as it get the data from url
    startFetching(Stock)
        # threading.Timer(0.5, startFetching,[Stock]).start()



# In[24]:
allthreads=[]

# In[8]:

def StockActions(Stock):
    initialize(Stock)
    startFetching(Stock)

def RunThread(Stock):
    global allthreads
    t1 = threading.Thread(target=StockActions, args=(Stock,)) 
    t1.start()
    allthreads.append(t1)
    
def RunProcessThreadSplits(StockList):
    global allthreads

    for Stock in StockList:
        t1 = threading.Thread(target=StockActions, args=(Stock,)) 
        t1.start()
        allthreads.append(t1)
    
    for th in allthreads:
        th.join()

def RunProcessThread(Stock):
    global allthreads
    t1 = threading.Thread(target=StockActions, args=(Stock,)) 
    t1.start()
    allthreads.append(t1)
    
    t1.join()



def RunThread_No(Stock):
    initialize(Stock)
    startFetching(Stock)

# def RunAStock(Stock):
#     print('created thread',Stock["Symbol"])
#     pr = Process(target=RunAThread, args=(Stock,))
#     pr.start()   


# In[25]:
# 

def withOnlyThreads():
    stockslist = getStocksInput()
    for stock in stockslist:
        RunThread(stock)
    
    for th in allthreads:
        th.join()


def withoutThreadsAndProcess():
    stockslist = getStocksInput()
    for stock in stockslist:
        StockActions(stock)

def withPoolOnly():
    stockslist = getStocksInput()
    p = Pool()
    p.map(RunThread_No, stockslist)
    p.close()
    print('closed')
    p.join()
    print('wait over')

def split_list(alist, wanted_parts=1):
    length = len(alist)
    return [ alist[i*length // wanted_parts: (i+1)*length // wanted_parts] 
             for i in range(wanted_parts) ]


def withPoolAndThreadsWithSplit():
    # stockslist = getStocksInput()
    # splitting
    allstockslist = getStocksInput()
    alist,blist,clist,dlist = split_list(allstockslist,4)
    stockslist=[alist,blist,clist,dlist]
    # splitting ends
    p = Pool()
    p.map(RunProcessThreadSplits, stockslist)
    p.close()
    print('closed')
    p.join()
    print('wait over')

def withPoolAndThreads():
    stockslist = getStocksInput()
    p = Pool()
    p.map(RunProcessThread, stockslist)
    p.close()
    print('closed')
    p.join()
    print('wait over')

processlist = []

def withProcessAndThreads():
    global processlist
    stockslist = getStocksInput()
    for stock in stockslist:
        p = Process(target=RunProcessThread, args=(stock,))
        p.start()
        
        # p = Pool(4)
        # p.map(RunProcessThread, stockslist)
        # p.close()
        # print('closed')
    for ps in processlist:
        ps.join()
        # print('wait over')

if __name__ == '__main__':
    # 9 seconds per symbol - for 3 symbols
    # 20 seconds for 25 symbols - requests error handling needs to be added
    # withOnlyThreads()


    # 6 seconds for 3 symbols
    # 25 symbols, but only 4 symbols loaded (pool default without param) , 6 seconds
    # 25 symbols, pool params added - 25 symbols loaded - taking 23 seconds for each
    #25 symbols, pool paramas removed, fetching loop removed - loaded all 25 symbols loaded, 
    #   because no busy threads after one startFetching function, just test to see 25 folders, no logs wil be generated
    # withPoolAndThreads()

    # split number of stocks in 4 chunks , 25 symbols - 25 seconds for each symbol
    withPoolAndThreadsWithSplit()

    # withoutThreadsAndProcess()

    # 6 seconds for 3 symbols
    # 25 symbols - giving error
    # withPoolOnly()

    # 6 seconds for 3 symbols - ctrl c working
    # 27 seconds for each 25 symbols
    # withProcessAndThreads()

    
        
    
    

# print('reading stock input file..')
# # for Stock in getStocksInput():
# #     p = Pool(4)
# #     print(p.map(f, [1, 2, 3]))
#     # t1 = threading.Thread(target=RunAStock, args=(Stock,)) 
#     # print('started thread',Stock["Symbol"])
#     # t1.start() 
# #     RunAStock(Stock)

