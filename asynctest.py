import asyncio



async def onemore():
    print('one more')

def hello():
    for _ in range(10000):
        await onemore()


async def main():
    print("param")
    hello()


asyncio.run(main())