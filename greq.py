import grequests
import time
import requests
import threading
import asyncio

class Test:
    def __init__(self):
        self.allthreads=[]
        self.urls = [
            'https://www.investing.com/equities/itc',
            'https://www.investing.com/equities/tcs',
            'https://www.investing.com/equities/hdfcbank',
            'https://www.investing.com/equities/yesbank',
            'https://www.investing.com/equities/google',
            'https://www.investing.com/equities/itc',
            'https://www.investing.com/equities/itc',
            'https://www.investing.com/equities/itc',
            'https://www.investing.com/equities/itc',
            'https://www.investing.com/equities/itc',
            'https://www.investing.com/equities/itc',
            'https://www.investing.com/equities/itc',
            'https://www.investing.com/equities/itc',
            'https://www.investing.com/equities/itc',
            'https://www.investing.com/equities/itc',
            'https://www.investing.com/equities/itc',
            'https://www.investing.com/equities/itc',
            'https://www.investing.com/equities/itc',
            'https://www.investing.com/equities/itc',
            'https://www.investing.com/equities/itc',
            'https://www.investing.com/equities/itc',
            'https://www.investing.com/equities/itc',
            'https://www.investing.com/equities/itc',
            'https://www.investing.com/equities/itc',
            'https://www.investing.com/equities/itc',
        ]

    def exception(self, request, exception):
        print "Problem: {}: {}".format(request.url, exception)

    def asyncz(self):
        starttime = time.time()
        results = grequests.map((grequests.get(u) for u in self.urls), exception_handler=self.exception, size=5)
        endtime = time.time()
        totaltime=endtime-starttime
        print ("total time is",totaltime)
        print results

    async def mynew(self):
        print('eys')
        pass

    def requestthis(self):
        headers = {'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Safari/605.1.15'}
        res = requests.get('https://www.investing.com/equities/itc', headers=headers, timeout=60)
        print('done')

    
    def another(self):
        self.mynew()
        print('starting')
        starttime = time.time()
        for u in self.urls:
            t1 = threading.Thread(target=self.requestthis) 
            t1.start()
            self.allthreads.append(t1)

        for th in self.allthreads:
            th.join()

        endtime = time.time()
        totaltime=endtime-starttime
        print ("total time is",totaltime)
        

test = Test()
test.another()

# taking 35 seconds for 25 urls